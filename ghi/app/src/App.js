import AttendeesList from './AttendeesList';
import Nav from './Nav';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <>
      <Nav />
      <div className="container">
        <AttendeesList attendees={props.attendees} />
      </div>
    </>
  );
}

export default App;
